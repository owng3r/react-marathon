import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";

import { firebaseConfig } from "../config";

class Firebase {
	constructor() {
		firebase.initializeApp(firebaseConfig);

		this.auth = firebase.auth();
		this.database = firebase.database();
		this.userUID = null;
	}

	setUserUID = (uid) => {
		this.userUID = uid;
	};

	signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
	createNewUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);
	getUserCardsRef = () => this.database.ref(`/cards/${this.userUID}`);
	getUserCurrentCardRef = (id) => this.database.ref(`/cards/${this.userUID}/${id}`);
}

export default Firebase;
