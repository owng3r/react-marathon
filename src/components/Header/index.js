import React from 'react';
import { Layout } from 'antd';

const { Header } = Layout;

const Head = ({ children }) => {
  return (
    <Layout>
      <Header>
        {children}
      </Header>
    </Layout>
  );
}

export default Head;