import React from 'react'
import style from './Heading.module.scss';

const Heading = ({ level, text }) => {
  const validHeaderLevels = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
  const Heading = validHeaderLevels.includes(level)
    ? level.toLowerCase()
    : '';

  return <Heading className={style.header}>{text}</Heading>
}

export default Heading;