import FirebaseContext from "../../context/FirebaseContext";

import React, { Component } from "react";
import HomePage from "../../pages/Home";
import AboutPage from "../../pages/About";
import LoginPage from "../../pages/Login";
import RegistrationPage from "../../pages/Registration";
import CurrentCardPage from "../../pages/CurrentCard";
import { addUserAction } from '../../store/actions/userAction';
import { connect } from 'react-redux';
import { PrivateRoute } from "../../utils/privateRoute";
import { Route, Switch, Redirect } from "react-router-dom";
import { Spin, Row, Col } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { bindActionCreators } from "redux";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

class App extends Component {
	componentDidMount() {
		const { auth, setUserUID } = this.context;
		const { addUser } = this.props;
		auth.onAuthStateChanged((user) => {
			if (user) {
				setUserUID(user.uid);
				localStorage.setItem("user", user.uid);
				addUser(user);
			} else {
				setUserUID(null);
				localStorage.removeItem("user");
			}
		});
	}

	render() {
		const { userUid } = this.props;

		if (!userUid) {
			return (
				<Row justify="center" align="middle" className="w-100 h-100">
					<Col span={12} className="text-center">
						<Spin indicator={antIcon} />
					</Col>
				</Row>
			);
		}

		return (
			<Switch>
				<Route path="/login" component={LoginPage} />
				<Route path="/registration" component={RegistrationPage} />
				<PrivateRoute path="/" exact component={HomePage} />
				<PrivateRoute path="/home/:id?/:isDone?" component={HomePage} />
				<PrivateRoute path="/word/:id?" component={CurrentCardPage} />
				<PrivateRoute path="/about" component={AboutPage} />
				<Redirect to="/" />
			</Switch>
		);
	}
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => {
	return {
		userUid: state.user.userUid
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		addUser: addUserAction
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
