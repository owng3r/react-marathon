import React from 'react';
import style from './Footer.module.scss';

const Footer = ({ children }) => {
  const date = new Date();

  const currentDate = new Intl
    .DateTimeFormat('ru-RU', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    })
    .format(date);

  return (
    <footer className={style.footer}>
      {children}
      <div className={style.copyright}>
        &copy; Aspire Residential Public, {currentDate}
      </div>
    </footer>
  )
}

export default Footer;