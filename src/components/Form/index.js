import React from 'react';
import style from './Form.module.scss';

const Form = ({ children, callbackFn }) => {
  return <form className={style.root} method="GET" onSubmit={(event) => callbackFn(event)}>{children}</form>
}

export default Form;