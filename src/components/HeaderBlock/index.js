import React from 'react';
import style from './HeaderBlock.module.scss';

const HeaderBlock = ({ hideBackground = false, children }) => {
  const additionalStyle = hideBackground
    ? { backgroundImage: 'none' }
    : {};

  return (
    <div className={style.cover} style={additionalStyle}>
      <div className={style.wrap}>
        {children}
      </div>
    </div>
  )
}

export default HeaderBlock;