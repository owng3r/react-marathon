import React from 'react';
import style from './Button.module.scss';

const Button = ({ text = '' }) => <button className={style.root}>{text}</button>

export default Button;