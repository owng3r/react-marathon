import React, { Component } from "react";
import cl from "classnames";
import style from "./Card.module.scss";
import { CheckCircleOutlined, DeleteOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";

class Card extends Component {
	state = {
		isOpened: false,
		isRemembered: false,
	};

	componentDidMount() {
		const {
			match: { params },
			index,
		} = this.props;

		if (index === parseInt(params.id)) {
			this.setState(() => ({
				isOpened: params.isDone,
			}));
		}
	}

	handleCardClick = (currentValue) => {
		this.setState(({ isOpened }) => ({
			isOpened: currentValue || !isOpened,
		}));
	};

	handleIsRememberClick = () => {
		this.setState(({ isRemembered }) => ({
			isOpened: !isRemembered,
			isRemembered: !isRemembered,
		}));
	};

	handleDeleteWord = () => {
		this.props.onDelete();
	};

	render() {
		const { eng, rus } = this.props;
		const { isOpened, isRemembered } = this.state;

		return (
			<div className={style.root}>
				<div className={cl(style.card, { [style.done]: isOpened, [style.remembered]: isRemembered })} onClick={() => this.handleCardClick(isOpened)}>
					<div className={style.cardInner}>
						<div className={style.cardFront}>{eng}</div>
						<div className={style.cardBack}>{rus}</div>
					</div>
				</div>

				<CheckCircleOutlined className={style.icon} onClick={this.handleIsRememberClick} />

				<DeleteOutlined className={style.icon} onClick={this.handleDeleteWord} />
			</div>
		);
	}
}

export default withRouter(Card);
