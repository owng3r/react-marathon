import React from 'react';
import style from './Paragraph.module.scss';

const Paragraph = ({ text }) => <p className={style.paragraph}>{text}</p>

export default Paragraph;