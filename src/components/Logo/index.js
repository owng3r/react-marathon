import React from 'react';
import style from './Logo.module.scss';
import { ReactComponent as SchoolLogo } from '../../assets/img/logo.svg';

const Logo = ({ size, isMonochrome = false }) => {
  const sizes = ['xs', 's', 'm', 'l', 'xl'];
  const validSize = sizes.includes(size) ? size.toLowerCase() : 'm';

  const classes = [style.logo, style[validSize]];

  if (isMonochrome)
    classes.push(style.monochrome);

  return (
    <div className={classes.join(' ')}>
      <SchoolLogo />
    </div>
  )
}

export default Logo;