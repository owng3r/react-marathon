import React from "react";
import Card from "../Card";
import { Row, Col } from "antd";

const Cards = ({ items = [], onDeleteWord }) => {
	return (
		<Row gutter={[16, 32]}>
			{items.map(({ eng, rus, id }, index) => (
				<Col key={id} className="gutter-row" span={12}>
					<Card id={id} index={index} eng={eng} rus={rus} onDelete={() => onDeleteWord(id)} />
				</Col>
			))}
		</Row>
	);
};

export default Cards;
