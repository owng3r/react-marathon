import React from 'react';
import style from './Content.module.scss';

const Content = ({ children }) => <main className={style.content} role="main">{children}</main>

export default Content;