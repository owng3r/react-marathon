import React from 'react';
import style from './Input.module.scss';

const Input = ({ value, onInput }) => {
  return (
    <label className={style.label}>
      <input
        className={style.root}
        value={value}
        type="text"
        placeholder="Введите слово и его перевод через запятую"
        onChange={(event) => onInput(event)} />
    </label>
  )
}

export default Input;