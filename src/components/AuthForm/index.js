import React from 'react';
import { Col, Form, Input, PageHeader, Row } from 'antd';

const AuthForm = ({ children, caption, onFinish = () => null, onFinishFailed = () => null }) => {
  const layout = {
    labelAlign: "left",
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
  };

  const tailLayout = {
    wrapperCol: { offset: 4, span: 20 },
  };

  return (
    <Row justify="center" align="middle" className="w-100 h-100">
      <Col span={12}>
        <PageHeader
          title={caption}
        />
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="E-mail"
            name="email"
            placehonder="example@email.com"
            rules={[{ required: true, message: 'Введите E-mail для регистрации' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Пароль"
            name="password"
            rules={[{ required: true, message: 'Введите пароль' }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            {children}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}

export default AuthForm;