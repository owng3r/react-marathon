const API_KEY = process.env.REACT_APP_YANDEX_API_KEY;
const YANDEX_URL = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup';
const TRANSLATE_LANG_PAIR = 'en-ru';

async function execute(url = '') {
  try {
    const response = await fetch(url, {
      method: 'GET',
      cors: 'no-cors',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return response;
  } catch (error) {
    throw new Error(error);
  }
}

async function parse (fetchResponse) {
  try {
    const result = await fetchResponse.json();
    return result;
  } catch (error) {
    throw new Error(error);
  }
}

export async function handleFetchRequest (textForFind = '') {
  const urlParams = new URLSearchParams({ key: API_KEY, lang: TRANSLATE_LANG_PAIR, text: textForFind }).toString();
  const url = `${YANDEX_URL}?${urlParams}`;

  const result = await execute(url);
  const json = await parse(result);

  return json;
}
