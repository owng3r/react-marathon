import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

import { firebaseConfig } from '../config';

export const fire = firebase;

fire.initializeApp(firebaseConfig);

export const database = fire.database();