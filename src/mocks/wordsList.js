import { v4 as uuidv4 } from 'uuid';

export const wordsList = [{
    id: uuidv4(),
    eng: 'between',
    rus: 'между'
}, {
    id: uuidv4(),
    eng: 'high',
    rus: 'высокий'
}, {
    id: uuidv4(),
    eng: 'really',
    rus: 'действительно'
}, {
    id: uuidv4(),
    eng: 'something',
    rus: 'что-нибудь'
}, {
    id: uuidv4(),
    eng: 'most',
    rus: 'большинство'
}, {
    id: uuidv4(),
    eng: 'another',
    rus: 'другой'
}, {
    id: uuidv4(),
    eng: 'much',
    rus: 'много'
}, {
    id: uuidv4(),
    eng: 'family',
    rus: 'семья'
}, {
    id: uuidv4(),
    eng: 'own',
    rus: 'личный'
}, {
    id: uuidv4(),
    eng: 'out',
    rus: 'из/вне'
}, {
    id: uuidv4(),
    eng: 'leave',
    rus: 'покидать'
}, {
    id: uuidv4(),
    eng: 'headphones',
    rus: 'наушники'
}
];
