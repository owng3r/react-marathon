import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import FirebaseContext from "./context/FirebaseContext";
import Firebase from "./services/Firebase";
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import rootReducer from './store/reducers';
import { BrowserRouter } from "react-router-dom";
import "antd/dist/antd.css";
import "./index.css";

const rootElement = document.getElementById("root");

const store = new createStore(rootReducer, applyMiddleware(thunk, logger));

ReactDOM.render(
	<Provider store={store}>
		<FirebaseContext.Provider value={new Firebase()}>
			<BrowserRouter>
				<App />
			</BrowserRouter>
		</FirebaseContext.Provider>
	</Provider>,
	rootElement
);
