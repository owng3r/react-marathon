import {
    ADD_NEW_WORD,
    FETCH_CARDS_LIST,
    FETCH_CARDS_LIST_RESOLVE,
    FETCH_CARDS_LIST_REJECT,
    REMOVE_WORD
} from '../types';

export const fetchCardList = (getData) => {
    return async (dispatch, getState) => {
        dispatch(cardListAction);
        try {
            fetchCardList();
            await getData().on("value", (response) => {
                dispatch(cardListResolveAction(response.val()));
            });
        } catch (error) {
            dispatch(cardListRejectAction(error));
        }
    }
}

export const cardListAction = () => ({
    type: FETCH_CARDS_LIST
});

export const cardListResolveAction = (payload) => ({
    type: FETCH_CARDS_LIST_RESOLVE,
    payload
});

export const cardListRejectAction = (err) => ({
    type: FETCH_CARDS_LIST_REJECT,
    err
});

export const setNewWordAction = (payload) => ({
    type: ADD_NEW_WORD,
    payload
});

export const removeWordAction = (payload) => ({
    type: REMOVE_WORD,
    payload
})