import { ADD_USER } from '../types';

export const addUserAction = (user) => ({
    type: ADD_USER,
    user
})