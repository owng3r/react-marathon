import { ADD_NEW_WORD, FETCH_CARDS_LIST, FETCH_CARDS_LIST_RESOLVE, FETCH_CARDS_LIST_REJECT, REMOVE_WORD } from '../types';

const cardListReducer = (state, action) => {
    switch (action.type) {
        case ADD_NEW_WORD:
        case REMOVE_WORD: {
            return {
                payload: action.payload,
                err: null,
                isLoading: false
            }
        }
        case FETCH_CARDS_LIST:
            return {
                payload: [],
                err: null,
                isLoading: true
            }
        case FETCH_CARDS_LIST_RESOLVE:
            return {
                payload: action.payload,
                err: null,
                isLoading: false
            }
        case FETCH_CARDS_LIST_REJECT:
            return {
                payload: null,
                err: action.err,
                isLoading: false
            }
        default:
            return {
                payload: null,
                err: null,
                isLoading: false
            }
    }
}

export default cardListReducer;