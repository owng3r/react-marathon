import { ADD_USER } from '../types';

const userReducer = (state = {}, { type, user }) => {
    switch (type) {
        case ADD_USER: {
            return {
                ...state,
                name: user.displayName,
                userUid: user.uid
            }
        }
        default:
            return state;
    }
}

export default userReducer;