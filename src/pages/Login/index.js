import React, { Component } from "react";
import AuthForm from "../../components/AuthForm";
import FirebaseContext from "../../context/FirebaseContext";
import { Button, notification, Space } from "antd";

class LoginPage extends Component {
	onFinish = ({ email, password }) => {
		const { signWithEmail, setUserUID } = this.context;

		signWithEmail(email, password)
			.then((res) => {
				const { history } = this.props;
				setUserUID(res.user.uid);
				localStorage.setItem("user", res.user.uid);
				history.push("/");
			})
			.catch(() => {
				notification["error"]({
					message: "Ошибка",
					description: "Неверный логин или пароль",
				});
			});
	};

	goToRegistrationPage = () => {
		const { history } = this.props;
		history.push("/registration");
	};

	render() {
		return (
			<AuthForm caption="Авторизация" onFinish={this.onFinish}>
				<Space>
					<Button type="primary" htmlType="submit">
						Войти
					</Button>
					<Button type="default" htmlType="button" onClick={this.goToRegistrationPage}>
						Регистрация
					</Button>
				</Space>
			</AuthForm>
		);
	}
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
