import React, { Component } from "react";
import AuthForm from "../../components/AuthForm";
import FirebaseContext from "../../context/FirebaseContext";
import { Button, notification, Space } from "antd";

class Registration extends Component {
	onFinish = ({ email, password }) => {
		const { createNewUser, setUserUID } = this.context;

		createNewUser(email, password)
			.then((res) => {
				setUserUID(res.user.uid);
				localStorage.setItem("user", JSON.stringify(res.user.uid));
				const { history } = this.props;
				history.push("/");
			})
			.catch(() => {
				notification["error"]({
					message: "Ошибка",
					description: `Пользователь с таким E-mail уже зарегистрирован. Пожалуйста, повторите попытку`,
				});
			});
	};

	goToLoginPage = () => {
		const { history } = this.props;
		history.push("/login");
	};

	render() {
		return (
			<AuthForm caption="Регистрация" onFinish={this.onFinish}>
				<Space>
					<Button type="primary" htmlType="submit">
						Зарегистрироваться
					</Button>
					<Button type="default" htmlType="button" onClick={this.goToLoginPage}>
						Назад
					</Button>
				</Space>
			</AuthForm>
		);
	}
}

Registration.contextType = FirebaseContext;

export default Registration;
