import React, { Component } from "react";
import style from "./Home.module.scss";

import { v4 as uuidv4 } from "uuid";
import { Button, Col, Input as AntdInput, Row, Spin } from "antd";
import { handleFetchRequest } from "../../utils/handleFetchRequest";
import { connect } from 'react-redux';
import { fetchCardList, removeWordAction, setNewWordAction } from '../../store/actions/cardListAction';
import { bindActionCreators } from "redux";

import HeaderBlock from "../../components/HeaderBlock";
import Heading from "../../components/Heading";
import Paragraph from "../../components/Paragraph";
import Cards from "../../components/Cards";
import Header from "../../components/Header";
import Content from "../../components/Content";
import Footer from "../../components/Footer";
import Logo from "../../components/Logo";
import FirebaseContext from "../../context/FirebaseContext";

const { Search } = AntdInput;

class HomePage extends Component {
	database = this.context.database;
	userCardsRef = this.context.getUserCardsRef;

	async componentDidMount() {
		await this.fetchData();
	}

	fetchData = async () => {
		const { fetchCardList } = this.props;
		await fetchCardList(this.userCardsRef);
	};

	handleDeleteWord = async (wordId = null) => {
		const { wordsList, removeWord } = this.props;
		const filteredWordsList = wordsList.filter(({ id }) => id !== wordId);

		await this.userCardsRef().set(filteredWordsList);
		removeWord(filteredWordsList);
	};

	handleAddWord = async (searchedEnglishWord = "") => {
		try {
			const { wordsList, setNewWord } = this.props;
			const { def } = await handleFetchRequest(searchedEnglishWord);

			if (def.length === 0) return;

			const [foundResult] = def;
			const [translateResult] = foundResult.tr;

			if (translateResult && translateResult.text) {
				const updatedWordsList = [
					...wordsList,
					{
						id: uuidv4(),
						eng: searchedEnglishWord,
						rus: translateResult.text,
					},
				];
				await this.userCardsRef().set(wordsList);
				setNewWord(updatedWordsList);
			}
		} catch (error) {
			console.error(error);
		}
	};

	onLogOut = () => {
		const { auth } = this.context;

		auth.signOut().then(() => {
			const { history } = this.props;
			localStorage.removeItem("user");
			history.push("/login");
		});
	};

	render() {
		const { isLoading, wordsList } = this.props;
		return (
			<>
				<Header>
					<Row className="w-100">
						<Col span={12}>
							<Logo size="l" />
						</Col>
						<Col span={12} className="text-right">
							<Button type="primary" size="l" onClick={this.onLogOut}>
								Выйти
							</Button>
						</Col>
					</Row>
				</Header>
				<HeaderBlock>
					<Heading level="h1" text="Учите слова онлайн" />
					<Paragraph text="Используйте карточки для запоминания и пополняйте активный словарный запас" />
				</HeaderBlock>
				<Content>
					<div className={style.form}>
						<Search placeholder="Введите слова" enterButton="Поиск" loading={isLoading} size="large" onSearch={this.handleAddWord} />
					</div>
					<Spin spinning={isLoading}>
						<Cards items={wordsList} onDeleteWord={this.handleDeleteWord} />
					</Spin>
				</Content>
				<Footer>
					<Logo isMonochrome />
				</Footer>
			</>
		);
	}
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
	return {
		isLoading: state.cardList.isLoading,
		wordsList: state.cardList.payload || []
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		removeWord: removeWordAction,
		setNewWord: setNewWordAction,
		fetchCardList
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
