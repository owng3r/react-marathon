import React, { Component } from "react";
import FirebaseContext from "../../context/FirebaseContext";
import style from "./CurrentCard.module.scss";
import { Spin, Typography } from "antd";

const { Title, Paragraph } = Typography;

class CurrentCardPage extends Component {
	state = {
		word: {
			id: 0,
			eng: "",
			rus: "",
		},
	};

	componentDidMount() {
		const { getUserCurrentCardRef } = this.context;
		const {
			match: { params },
		} = this.props;

		if (params.id) {
			getUserCurrentCardRef(params.id)
				.once("value")
				.then((res) => {
					this.setState(() => ({
						word: res.val(),
					}));
				});
		}
	}

	render() {
		const { word } = this.state;

		if (!word || (word.eng === "" && word.rus === ""))
			return (
				<div className={style.root}>
					<Spin />
				</div>
			);

		return (
			<>
				<Title>{word.eng.toUpperCase()}</Title>
				<Paragraph>
					Ant Design, a design language for background applications, is refined by Ant UED Team. Ant Design, a design language for background applications, is
					refined by Ant UED Team. Ant Design, a design language for background applications, is refined by Ant UED Team. Ant Design, a design language for
					background applications, is refined by Ant UED Team. Ant Design, a design language for background applications, is refined by Ant UED Team. Ant
					Design, a design language for background applications, is refined by Ant UED Team.
				</Paragraph>
			</>
		);
	}
}

CurrentCardPage.contextType = FirebaseContext;

export default CurrentCardPage;
